﻿using Messanger.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Messanger
{
    public partial class SignupForm : Form
    {
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        public SignupForm()
        {
            InitializeComponent();
            CursorType();
        }

        #region// Custom Mouse Hover and leave
        private void buttonHover(object sender, EventArgs e)
        {
            Bitmap Bmt = new Bitmap((Resources.Icon9), 20, 20);
            this.Cursor = new Cursor(Bmt.GetHicon());
        }
        private void buttonLeave(object sender, EventArgs e)
        {
            Bitmap Bmt = new Bitmap((Resources.Icon9R), 20, 20);
            this.Cursor = new Cursor(Bmt.GetHicon());
        }
        private void CursorType()
        {
            Bitmap Bmt = new Bitmap((Resources.Icon9R), 20, 20);
            this.Cursor = new Cursor(Bmt.GetHicon());
        }
        #endregion

        #region// Dragable form
        private void SignupForm_MouseDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void SignupForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        private void SignupForm_MouseUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }
        #endregion

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            LoginForm login = new LoginForm();
            login.Show();
            this.Hide();
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TextBoxFirstName_TextChanged(object sender, EventArgs e)
        {
            textBoxUserName.Text = textBoxFirstName.Text;
        }

        private void ButtonSignup_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxFirstName.Text) && !string.IsNullOrEmpty(textBoxSurName.Text) && !string.IsNullOrEmpty(textBoxContactNo.Text) &&
               !string.IsNullOrEmpty(textBoxGmail.Text) && !string.IsNullOrEmpty(textBoxUserName.Text) && !string.IsNullOrEmpty(textBoxPassword.Text) && !string.IsNullOrEmpty(textBoxRetypePassword.Text))
            {
                if (textBoxPassword.Text==textBoxRetypePassword.Text)
                {
                    bool statement = Logical.CreateUser(textBoxFirstName.Text, textBoxMiddleName.Text, textBoxSurName.Text, dateTimePicker.Text, textBoxContactNo.Text, textBoxGmail.Text, textBoxUserName.Text, textBoxRetypePassword.Text);
                    if (statement)
                    {
                       DialogResult result= MessageBox.Show("Account created, Do you want to login?","Account Created",MessageBoxButtons.YesNo,MessageBoxIcon.Information,MessageBoxDefaultButton.Button2);
                        if (result==DialogResult.Yes)
                        {
                            MessageBox.Show("Messanger");
                        }
                        else
                        {
                            MessageBox.Show("Server Down");
                        }
                        textBoxFirstName.Clear();textBoxMiddleName.Clear();textBoxSurName.Clear();dateTimePicker.Value=DateTime.Now;
                        textBoxContactNo.Clear();textBoxGmail.Clear();textBoxUserName.Clear();textBoxPassword.Text=Logical.Generate();textBoxRetypePassword.Text = textBoxPassword.Text;
                    }
                }
                else
                {
                    MessageBox.Show("Password not matched!!");
                    textBoxPassword.Clear();
                    textBoxRetypePassword.Clear();
                    textBoxPassword.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please enter the details!!");
            }
        }

        private void SignupForm_Load(object sender, EventArgs e)
        {
            string password = Logical.Generate();
            textBoxPassword.Text = password;
            textBoxRetypePassword.Text = password;
        }

        private void CheckBoxShowHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
                textBoxRetypePassword.UseSystemPasswordChar = false;
                checkBoxShowHidePassword.Image = Resources.Hide;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
                textBoxRetypePassword.UseSystemPasswordChar = true;
                checkBoxShowHidePassword.Image = Resources.Show;
            }
        }
    }
}
