﻿namespace Messanger
{
    partial class MessangerPortal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLeft = new System.Windows.Forms.Panel();
            this.buttonViewProfile = new System.Windows.Forms.Button();
            this.buttonSharePC = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.paneltop = new System.Windows.Forms.Panel();
            this.labelOnline = new System.Windows.Forms.Label();
            this.labelDash = new System.Windows.Forms.Label();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonMaximize = new System.Windows.Forms.Button();
            this.labeluserName = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.panelButtom = new System.Windows.Forms.Panel();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.panelButtomRightSend = new System.Windows.Forms.Panel();
            this.buttonImage = new System.Windows.Forms.Button();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.buttonSend = new System.Windows.Forms.Button();
            this.panelBody = new System.Windows.Forms.Panel();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.richTextBoxDisplayMessage = new System.Windows.Forms.RichTextBox();
            this.buttonLogOff = new System.Windows.Forms.Button();
            this.labelId = new System.Windows.Forms.Label();
            this.panelBehind = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonUpdateInfo = new System.Windows.Forms.Button();
            this.groupBoxUserLogin = new System.Windows.Forms.GroupBox();
            this.buttonUpdateLogin = new System.Windows.Forms.Button();
            this.checkBoxShowHidePassword = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxRetypePassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxContactNo = new System.Windows.Forms.TextBox();
            this.textBoxGmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxSurName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxMiddleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.panelLeft.SuspendLayout();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.paneltop.SuspendLayout();
            this.panelButtom.SuspendLayout();
            this.panelButtomRightSend.SuspendLayout();
            this.panelBody.SuspendLayout();
            this.panelBehind.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxUserLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLeft.Controls.Add(this.buttonViewProfile);
            this.panelLeft.Controls.Add(this.buttonSharePC);
            this.panelLeft.Controls.Add(this.groupBox);
            this.panelLeft.Controls.Add(this.dataGridView);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(305, 493);
            this.panelLeft.TabIndex = 10;
            // 
            // buttonViewProfile
            // 
            this.buttonViewProfile.BackColor = System.Drawing.Color.Maroon;
            this.buttonViewProfile.FlatAppearance.BorderSize = 0;
            this.buttonViewProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.buttonViewProfile.ForeColor = System.Drawing.Color.White;
            this.buttonViewProfile.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonViewProfile.Location = new System.Drawing.Point(6, 5);
            this.buttonViewProfile.Name = "buttonViewProfile";
            this.buttonViewProfile.Size = new System.Drawing.Size(140, 29);
            this.buttonViewProfile.TabIndex = 2;
            this.buttonViewProfile.Text = "Profile";
            this.buttonViewProfile.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonViewProfile.UseVisualStyleBackColor = false;
            this.buttonViewProfile.Click += new System.EventHandler(this.ButtonViewProfile_Click);
            // 
            // buttonSharePC
            // 
            this.buttonSharePC.BackColor = System.Drawing.Color.Tomato;
            this.buttonSharePC.FlatAppearance.BorderSize = 0;
            this.buttonSharePC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSharePC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.buttonSharePC.ForeColor = System.Drawing.Color.White;
            this.buttonSharePC.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonSharePC.Location = new System.Drawing.Point(156, 5);
            this.buttonSharePC.Name = "buttonSharePC";
            this.buttonSharePC.Size = new System.Drawing.Size(140, 29);
            this.buttonSharePC.TabIndex = 6;
            this.buttonSharePC.Text = "Share PC";
            this.buttonSharePC.UseVisualStyleBackColor = false;
            // 
            // groupBox
            // 
            this.groupBox.BackColor = System.Drawing.Color.Black;
            this.groupBox.Controls.Add(this.label1);
            this.groupBox.Controls.Add(this.textBoxSearch);
            this.groupBox.Location = new System.Drawing.Point(0, 32);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(302, 44);
            this.groupBox.TabIndex = 5;
            this.groupBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(198)))), ((int)(((byte)(7)))));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Search";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.textBoxSearch.Location = new System.Drawing.Point(94, 11);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(202, 27);
            this.textBoxSearch.TabIndex = 3;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.TextBoxSearch_TextChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.BackgroundColor = System.Drawing.Color.Black;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView.ColumnHeadersVisible = false;
            this.dataGridView.Location = new System.Drawing.Point(3, 76);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(297, 415);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellClick);
            // 
            // paneltop
            // 
            this.paneltop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paneltop.Controls.Add(this.buttonLogOff);
            this.paneltop.Controls.Add(this.labelId);
            this.paneltop.Controls.Add(this.labelOnline);
            this.paneltop.Controls.Add(this.labelDash);
            this.paneltop.Controls.Add(this.buttonMinimize);
            this.paneltop.Controls.Add(this.buttonMaximize);
            this.paneltop.Controls.Add(this.labeluserName);
            this.paneltop.Controls.Add(this.buttonExit);
            this.paneltop.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneltop.Location = new System.Drawing.Point(305, 0);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(860, 37);
            this.paneltop.TabIndex = 11;
            this.paneltop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Paneltop_MouseDown);
            this.paneltop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Paneltop_MouseMove);
            this.paneltop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Paneltop_MouseUp);
            // 
            // labelOnline
            // 
            this.labelOnline.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOnline.AutoSize = true;
            this.labelOnline.BackColor = System.Drawing.Color.Black;
            this.labelOnline.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold);
            this.labelOnline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(198)))), ((int)(((byte)(7)))));
            this.labelOnline.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelOnline.Location = new System.Drawing.Point(16, 8);
            this.labelOnline.Name = "labelOnline";
            this.labelOnline.Size = new System.Drawing.Size(63, 20);
            this.labelOnline.TabIndex = 9;
            this.labelOnline.Text = "Online";
            // 
            // labelDash
            // 
            this.labelDash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDash.AutoSize = true;
            this.labelDash.BackColor = System.Drawing.Color.Black;
            this.labelDash.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold);
            this.labelDash.ForeColor = System.Drawing.Color.DarkOrange;
            this.labelDash.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelDash.Location = new System.Drawing.Point(85, 9);
            this.labelDash.Name = "labelDash";
            this.labelDash.Size = new System.Drawing.Size(16, 20);
            this.labelDash.TabIndex = 8;
            this.labelDash.Text = "-";
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMinimize.BackColor = System.Drawing.Color.Black;
            this.buttonMinimize.BackgroundImage = global::Messanger.Properties.Resources.MinusRed;
            this.buttonMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMinimize.FlatAppearance.BorderSize = 0;
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonMinimize.Location = new System.Drawing.Point(732, 3);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(34, 31);
            this.buttonMinimize.TabIndex = 7;
            this.buttonMinimize.Text = " ";
            this.buttonMinimize.UseVisualStyleBackColor = false;
            this.buttonMinimize.Click += new System.EventHandler(this.ButtonMinimize_Click);
            // 
            // buttonMaximize
            // 
            this.buttonMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMaximize.BackColor = System.Drawing.Color.Black;
            this.buttonMaximize.BackgroundImage = global::Messanger.Properties.Resources.AddRed;
            this.buttonMaximize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMaximize.FlatAppearance.BorderSize = 0;
            this.buttonMaximize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMaximize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonMaximize.Location = new System.Drawing.Point(772, 3);
            this.buttonMaximize.Name = "buttonMaximize";
            this.buttonMaximize.Size = new System.Drawing.Size(34, 31);
            this.buttonMaximize.TabIndex = 6;
            this.buttonMaximize.Text = " ";
            this.buttonMaximize.UseVisualStyleBackColor = false;
            this.buttonMaximize.Click += new System.EventHandler(this.ButtonMaximize_Click);
            // 
            // labeluserName
            // 
            this.labeluserName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeluserName.AutoSize = true;
            this.labeluserName.BackColor = System.Drawing.Color.Black;
            this.labeluserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold);
            this.labeluserName.ForeColor = System.Drawing.Color.DarkOrange;
            this.labeluserName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labeluserName.Location = new System.Drawing.Point(107, 9);
            this.labeluserName.Name = "labeluserName";
            this.labeluserName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labeluserName.Size = new System.Drawing.Size(57, 20);
            this.labeluserName.TabIndex = 5;
            this.labeluserName.Text = "Name";
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.BackColor = System.Drawing.Color.Black;
            this.buttonExit.BackgroundImage = global::Messanger.Properties.Resources.X_Red;
            this.buttonExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonExit.FlatAppearance.BorderSize = 0;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonExit.Location = new System.Drawing.Point(812, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(34, 31);
            this.buttonExit.TabIndex = 0;
            this.buttonExit.Text = " ";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // panelButtom
            // 
            this.panelButtom.BackColor = System.Drawing.Color.Black;
            this.panelButtom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelButtom.Controls.Add(this.textBoxMessage);
            this.panelButtom.Controls.Add(this.panelButtomRightSend);
            this.panelButtom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButtom.Location = new System.Drawing.Point(305, 415);
            this.panelButtom.Name = "panelButtom";
            this.panelButtom.Size = new System.Drawing.Size(860, 78);
            this.panelButtom.TabIndex = 12;
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.BackColor = System.Drawing.Color.Black;
            this.textBoxMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.textBoxMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(198)))), ((int)(((byte)(7)))));
            this.textBoxMessage.Location = new System.Drawing.Point(0, 0);
            this.textBoxMessage.MaxLength = 21474;
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(732, 76);
            this.textBoxMessage.TabIndex = 0;
            this.textBoxMessage.TabStop = false;
            // 
            // panelButtomRightSend
            // 
            this.panelButtomRightSend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelButtomRightSend.Controls.Add(this.buttonImage);
            this.panelButtomRightSend.Controls.Add(this.buttonUpload);
            this.panelButtomRightSend.Controls.Add(this.buttonSend);
            this.panelButtomRightSend.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelButtomRightSend.Location = new System.Drawing.Point(732, 0);
            this.panelButtomRightSend.Name = "panelButtomRightSend";
            this.panelButtomRightSend.Size = new System.Drawing.Size(126, 76);
            this.panelButtomRightSend.TabIndex = 0;
            // 
            // buttonImage
            // 
            this.buttonImage.AccessibleDescription = "Image";
            this.buttonImage.AccessibleName = "Image";
            this.buttonImage.BackColor = System.Drawing.Color.DarkBlue;
            this.buttonImage.BackgroundImage = global::Messanger.Properties.Resources.Image;
            this.buttonImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonImage.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.buttonImage.ForeColor = System.Drawing.Color.Transparent;
            this.buttonImage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonImage.Location = new System.Drawing.Point(0, 41);
            this.buttonImage.Name = "buttonImage";
            this.buttonImage.Size = new System.Drawing.Size(69, 33);
            this.buttonImage.TabIndex = 2;
            this.buttonImage.Tag = "Image";
            this.buttonImage.UseVisualStyleBackColor = false;
            // 
            // buttonUpload
            // 
            this.buttonUpload.AccessibleDescription = "Upload";
            this.buttonUpload.AccessibleName = "Upload";
            this.buttonUpload.BackColor = System.Drawing.Color.Red;
            this.buttonUpload.BackgroundImage = global::Messanger.Properties.Resources.Upload;
            this.buttonUpload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonUpload.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.buttonUpload.ForeColor = System.Drawing.Color.Transparent;
            this.buttonUpload.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonUpload.Location = new System.Drawing.Point(68, 41);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(56, 33);
            this.buttonUpload.TabIndex = 1;
            this.buttonUpload.Tag = "Uplaod";
            this.buttonUpload.UseVisualStyleBackColor = false;
            // 
            // buttonSend
            // 
            this.buttonSend.BackColor = System.Drawing.Color.Blue;
            this.buttonSend.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSend.ForeColor = System.Drawing.Color.White;
            this.buttonSend.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonSend.Location = new System.Drawing.Point(0, 0);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(124, 41);
            this.buttonSend.TabIndex = 0;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = false;
            // 
            // panelBody
            // 
            this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBody.Controls.Add(this.richTextBox);
            this.panelBody.Controls.Add(this.richTextBoxDisplayMessage);
            this.panelBody.Location = new System.Drawing.Point(2, 5);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(857, 372);
            this.panelBody.TabIndex = 13;
            // 
            // richTextBox
            // 
            this.richTextBox.BackColor = System.Drawing.Color.Black;
            this.richTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.richTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(198)))), ((int)(((byte)(7)))));
            this.richTextBox.Location = new System.Drawing.Point(0, 0);
            this.richTextBox.MaxLength = 21474;
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(855, 370);
            this.richTextBox.TabIndex = 10;
            this.richTextBox.Text = "";
            // 
            // richTextBoxDisplayMessage
            // 
            this.richTextBoxDisplayMessage.BackColor = System.Drawing.Color.Black;
            this.richTextBoxDisplayMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBoxDisplayMessage.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBoxDisplayMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxDisplayMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.richTextBoxDisplayMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(198)))), ((int)(((byte)(7)))));
            this.richTextBoxDisplayMessage.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxDisplayMessage.MaxLength = 21474;
            this.richTextBoxDisplayMessage.Name = "richTextBoxDisplayMessage";
            this.richTextBoxDisplayMessage.ReadOnly = true;
            this.richTextBoxDisplayMessage.Size = new System.Drawing.Size(855, 370);
            this.richTextBoxDisplayMessage.TabIndex = 9;
            this.richTextBoxDisplayMessage.Text = "";
            // 
            // buttonLogOff
            // 
            this.buttonLogOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogOff.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonLogOff.FlatAppearance.BorderSize = 0;
            this.buttonLogOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogOff.ForeColor = System.Drawing.Color.OrangeRed;
            this.buttonLogOff.Location = new System.Drawing.Point(632, 2);
            this.buttonLogOff.Name = "buttonLogOff";
            this.buttonLogOff.Size = new System.Drawing.Size(94, 34);
            this.buttonLogOff.TabIndex = 36;
            this.buttonLogOff.Text = "Log off";
            this.buttonLogOff.UseVisualStyleBackColor = true;
            this.buttonLogOff.Click += new System.EventHandler(this.ButtonLogOff_Click);
            // 
            // labelId
            // 
            this.labelId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelId.AutoSize = true;
            this.labelId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelId.ForeColor = System.Drawing.Color.Yellow;
            this.labelId.Location = new System.Drawing.Point(589, 4);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(30, 25);
            this.labelId.TabIndex = 35;
            this.labelId.Text = "Id";
            // 
            // panelBehind
            // 
            this.panelBehind.BackColor = System.Drawing.Color.Black;
            this.panelBehind.Controls.Add(this.groupBox2);
            this.panelBehind.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBehind.Location = new System.Drawing.Point(305, 37);
            this.panelBehind.Name = "panelBehind";
            this.panelBehind.Size = new System.Drawing.Size(860, 378);
            this.panelBehind.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.panelBody);
            this.groupBox2.Controls.Add(this.buttonUpdateInfo);
            this.groupBox2.Controls.Add(this.groupBoxUserLogin);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dateTimePicker);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBoxContactNo);
            this.groupBox2.Controls.Add(this.textBoxGmail);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBoxSurName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBoxMiddleName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxFirstName);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(860, 378);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            // 
            // buttonUpdateInfo
            // 
            this.buttonUpdateInfo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonUpdateInfo.FlatAppearance.BorderSize = 0;
            this.buttonUpdateInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateInfo.ForeColor = System.Drawing.Color.OrangeRed;
            this.buttonUpdateInfo.Location = new System.Drawing.Point(288, 173);
            this.buttonUpdateInfo.Name = "buttonUpdateInfo";
            this.buttonUpdateInfo.Size = new System.Drawing.Size(312, 35);
            this.buttonUpdateInfo.TabIndex = 38;
            this.buttonUpdateInfo.Text = "Update user informations";
            this.buttonUpdateInfo.UseVisualStyleBackColor = true;
            this.buttonUpdateInfo.Click += new System.EventHandler(this.ButtonUpdateInfo_Click);
            // 
            // groupBoxUserLogin
            // 
            this.groupBoxUserLogin.Controls.Add(this.buttonUpdateLogin);
            this.groupBoxUserLogin.Controls.Add(this.checkBoxShowHidePassword);
            this.groupBoxUserLogin.Controls.Add(this.label10);
            this.groupBoxUserLogin.Controls.Add(this.textBoxRetypePassword);
            this.groupBoxUserLogin.Controls.Add(this.label8);
            this.groupBoxUserLogin.Controls.Add(this.textBoxPassword);
            this.groupBoxUserLogin.Controls.Add(this.label9);
            this.groupBoxUserLogin.Controls.Add(this.textBoxUserName);
            this.groupBoxUserLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxUserLogin.Location = new System.Drawing.Point(3, 214);
            this.groupBoxUserLogin.Name = "groupBoxUserLogin";
            this.groupBoxUserLogin.Size = new System.Drawing.Size(854, 161);
            this.groupBoxUserLogin.TabIndex = 39;
            this.groupBoxUserLogin.TabStop = false;
            this.groupBoxUserLogin.Visible = false;
            // 
            // buttonUpdateLogin
            // 
            this.buttonUpdateLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdateLogin.FlatAppearance.BorderSize = 0;
            this.buttonUpdateLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateLogin.ForeColor = System.Drawing.Color.OrangeRed;
            this.buttonUpdateLogin.Location = new System.Drawing.Point(285, 105);
            this.buttonUpdateLogin.Name = "buttonUpdateLogin";
            this.buttonUpdateLogin.Size = new System.Drawing.Size(340, 45);
            this.buttonUpdateLogin.TabIndex = 32;
            this.buttonUpdateLogin.Text = "Update login informations";
            this.buttonUpdateLogin.UseVisualStyleBackColor = true;
            this.buttonUpdateLogin.Click += new System.EventHandler(this.ButtonUpdateLogin_Click);
            // 
            // checkBoxShowHidePassword
            // 
            this.checkBoxShowHidePassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxShowHidePassword.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxShowHidePassword.AutoSize = true;
            this.checkBoxShowHidePassword.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxShowHidePassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.checkBoxShowHidePassword.FlatAppearance.BorderSize = 0;
            this.checkBoxShowHidePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxShowHidePassword.Image = global::Messanger.Properties.Resources.Show;
            this.checkBoxShowHidePassword.Location = new System.Drawing.Point(806, 18);
            this.checkBoxShowHidePassword.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxShowHidePassword.Name = "checkBoxShowHidePassword";
            this.checkBoxShowHidePassword.Size = new System.Drawing.Size(30, 30);
            this.checkBoxShowHidePassword.TabIndex = 37;
            this.checkBoxShowHidePassword.Text = " ";
            this.checkBoxShowHidePassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxShowHidePassword.UseVisualStyleBackColor = false;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.OrangeRed;
            this.label10.Location = new System.Drawing.Point(377, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(220, 40);
            this.label10.TabIndex = 36;
            this.label10.Text = "Retype Password :";
            // 
            // textBoxRetypePassword
            // 
            this.textBoxRetypePassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRetypePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRetypePassword.Location = new System.Drawing.Point(603, 62);
            this.textBoxRetypePassword.MaxLength = 20;
            this.textBoxRetypePassword.Name = "textBoxRetypePassword";
            this.textBoxRetypePassword.Size = new System.Drawing.Size(233, 30);
            this.textBoxRetypePassword.TabIndex = 33;
            this.textBoxRetypePassword.UseSystemPasswordChar = true;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.OrangeRed;
            this.label8.Location = new System.Drawing.Point(452, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 45);
            this.label8.TabIndex = 35;
            this.label8.Text = "Password :";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPassword.Location = new System.Drawing.Point(603, 21);
            this.textBoxPassword.MaxLength = 20;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(181, 30);
            this.textBoxPassword.TabIndex = 32;
            this.textBoxPassword.UseSystemPasswordChar = true;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.OrangeRed;
            this.label9.Location = new System.Drawing.Point(4, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(166, 25);
            this.label9.TabIndex = 34;
            this.label9.Text = "User Name :";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUserName.Location = new System.Drawing.Point(176, 21);
            this.textBoxUserName.MaxLength = 20;
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(214, 30);
            this.textBoxUserName.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.OrangeRed;
            this.label4.Location = new System.Drawing.Point(506, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 28);
            this.label4.TabIndex = 38;
            this.label4.Text = "Gmail :";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker.Location = new System.Drawing.Point(618, 75);
            this.dateTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(217, 30);
            this.dateTimePicker.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.OrangeRed;
            this.label6.Location = new System.Drawing.Point(438, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 28);
            this.label6.TabIndex = 23;
            this.label6.Text = "Date of Birth :";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.OrangeRed;
            this.label7.Location = new System.Drawing.Point(35, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 25);
            this.label7.TabIndex = 21;
            this.label7.Text = "Contact :";
            // 
            // textBoxContactNo
            // 
            this.textBoxContactNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxContactNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxContactNo.Location = new System.Drawing.Point(179, 126);
            this.textBoxContactNo.MaxLength = 20;
            this.textBoxContactNo.Name = "textBoxContactNo";
            this.textBoxContactNo.Size = new System.Drawing.Size(214, 30);
            this.textBoxContactNo.TabIndex = 4;
            // 
            // textBoxGmail
            // 
            this.textBoxGmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxGmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGmail.Location = new System.Drawing.Point(618, 125);
            this.textBoxGmail.MaxLength = 50;
            this.textBoxGmail.Name = "textBoxGmail";
            this.textBoxGmail.Size = new System.Drawing.Size(217, 30);
            this.textBoxGmail.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.OrangeRed;
            this.label5.Location = new System.Drawing.Point(23, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 25);
            this.label5.TabIndex = 17;
            this.label5.Text = "Surname :";
            // 
            // textBoxSurName
            // 
            this.textBoxSurName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxSurName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSurName.Location = new System.Drawing.Point(179, 76);
            this.textBoxSurName.MaxLength = 20;
            this.textBoxSurName.Name = "textBoxSurName";
            this.textBoxSurName.Size = new System.Drawing.Size(214, 30);
            this.textBoxSurName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.OrangeRed;
            this.label3.Location = new System.Drawing.Point(438, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 28);
            this.label3.TabIndex = 15;
            this.label3.Text = "Middle name :";
            // 
            // textBoxMiddleName
            // 
            this.textBoxMiddleName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMiddleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMiddleName.Location = new System.Drawing.Point(618, 18);
            this.textBoxMiddleName.MaxLength = 20;
            this.textBoxMiddleName.Name = "textBoxMiddleName";
            this.textBoxMiddleName.Size = new System.Drawing.Size(217, 30);
            this.textBoxMiddleName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.OrangeRed;
            this.label2.Location = new System.Drawing.Point(9, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 25);
            this.label2.TabIndex = 13;
            this.label2.Text = "First name :";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstName.Location = new System.Drawing.Point(179, 18);
            this.textBoxFirstName.MaxLength = 20;
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(214, 30);
            this.textBoxFirstName.TabIndex = 0;
            // 
            // MessangerPortal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1165, 493);
            this.ControlBox = false;
            this.Controls.Add(this.panelBehind);
            this.Controls.Add(this.panelButtom);
            this.Controls.Add(this.paneltop);
            this.Controls.Add(this.panelLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MessangerPortal";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MessangerPortal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MessangerPortal_FormClosing);
            this.panelLeft.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            this.panelButtom.ResumeLayout(false);
            this.panelButtom.PerformLayout();
            this.panelButtomRightSend.ResumeLayout(false);
            this.panelBody.ResumeLayout(false);
            this.panelBehind.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxUserLogin.ResumeLayout(false);
            this.groupBoxUserLogin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Button buttonViewProfile;
        private System.Windows.Forms.Button buttonSharePC;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Button buttonMaximize;
        private System.Windows.Forms.Label labeluserName;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Panel panelButtom;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Panel panelButtomRightSend;
        private System.Windows.Forms.Button buttonImage;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.RichTextBox richTextBoxDisplayMessage;
        private System.Windows.Forms.Label labelOnline;
        private System.Windows.Forms.Label labelDash;
        private System.Windows.Forms.Button buttonLogOff;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.Panel panelBehind;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonUpdateInfo;
        private System.Windows.Forms.GroupBox groupBoxUserLogin;
        private System.Windows.Forms.Button buttonUpdateLogin;
        private System.Windows.Forms.CheckBox checkBoxShowHidePassword;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxRetypePassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxContactNo;
        private System.Windows.Forms.TextBox textBoxGmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxSurName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxMiddleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFirstName;
    }
}