﻿using Messanger.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Messanger
{
    public partial class LoginForm : Form
    {
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        public LoginForm()
        {
            InitializeComponent();
            CursorType();
        }
        #region// Custom Mouse Hover and leave
        private void buttonHover(object sender, EventArgs e)
        {
            Bitmap Bmt = new Bitmap((Resources.Icon9), 20, 20);
            this.Cursor = new Cursor(Bmt.GetHicon());
        }
        private void buttonLeave(object sender, EventArgs e)
        {
            Bitmap Bmt = new Bitmap((Resources.Icon9R), 20, 20);
            this.Cursor = new Cursor(Bmt.GetHicon());
        }
        private void CursorType()
        {
            Bitmap Bmt = new Bitmap((Resources.Icon9R), 20, 20);
            this.Cursor = new Cursor(Bmt.GetHicon());
        }
        #endregion

        #region// Dragable Form
        private void LoginForm_MouseDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void LoginForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        private void LoginForm_MouseUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }

        #endregion

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxUserName.Text) && !string.IsNullOrEmpty(textBoxPassword.Text))
            {
                string ConditionCheck = Logical.CheckUserLogin(textBoxUserName.Text, textBoxPassword.Text);
                switch (ConditionCheck)
                {
                    case "Available_Success":
                        MessangerPortal portal = new MessangerPortal(textBoxUserName.Text);
                        portal.Show();
                        this.Hide();
                        break;
                    case "Available_Failure":
                        MessageBox.Show("Incorrect password");
                        break;
                    case "Unavailable _Failure":
                        MessageBox.Show("User Name not matched");
                        break;
                    default:
                        MessageBox.Show("Server Down");
                        break;
                }
            }
            else
            {
                MessageBox.Show("User Name or Password missing!!");
            }
        }

        private void ButtonSignup_Click(object sender, EventArgs e)
        {
           
            SignupForm signup = new SignupForm();
            signup.Show();
            this.Hide();
        }

        private void CheckBoxShowHidePassword_CheckedChanged(object sender, EventArgs e)
        {
             
            if (checkBoxShowHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
                checkBoxShowHidePassword.Image = Resources.Hide;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
                checkBoxShowHidePassword.Image = Resources.Show;
            }
        }
    }
}
