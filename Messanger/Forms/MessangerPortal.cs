﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Messanger
{
    public partial class MessangerPortal : Form
    {
        string LoggedinUser;
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        private bool isFirstselect = true;
        DataTable table;
        string SQLCON = System.Configuration.ConfigurationManager.ConnectionStrings["Connection"].ConnectionString.ToString();
        SqlDataAdapter SqlDataAdapter;
        public MessangerPortal(string userName)
        {
            InitializeComponent();
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            this.WindowState = FormWindowState.Normal;
            LoggedinUser = userName;
            labeluserName.Text = userName;
            IsAvailable(userName);
            ClearPanels(true);
            DisplayData();
            GetusersInformation();
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonMaximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
            }
            else if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void ButtonMinimize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized;
            }
            else if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Minimized;
            }
        }

        private void Paneltop_MouseDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void Paneltop_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        private void Paneltop_MouseUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }
        private void ClearPanels(bool ConditionCheck)
        {
            if (ConditionCheck)
            {
                //True
                panelBody.Hide();
                panelButtom.Hide();
                labelDash.Visible = false;
                labelOnline.Visible = false;
                labeluserName.Visible = false;
                textBoxMessage.Clear();
                richTextBox.Clear();
                buttonLogOff.Visible = true;
                labelId.Visible = false;
            }
            else
            {
                //False
                panelBody.Show();
                panelButtom.Show();
                labelDash.Visible = true;
                labelOnline.Visible = true;
                labeluserName.Visible = true;
                buttonLogOff.Visible = false;
                labelId.Visible = true;
            }
        }
        private void IsAvailable(string Username)
        {
            string UserId = Logical.GetUserId(Username);
            dataGridView.DataSource = Logical.GetTableByQuery("select FirstName+' '+MiddleName+' '+Surname,UserId from UserInformation where UserId!=" + UserId);
            dataGridView.Columns[1].Visible = false;
        }

        private void DisplayData()
        {
            string UserId = Logical.GetUserId(LoggedinUser);
            using (SqlConnection con = new SqlConnection(SQLCON))
            {
                con.Open();
                table = new DataTable();
                SqlDataAdapter = new SqlDataAdapter("select FirstName+' '+MiddleName+' '+Surname AS Name,UserId from UserInformation where UserId!=" + UserId, con);
                SqlDataAdapter.Fill(table);
                dataGridView.DataSource = table;
                dataGridView.Columns[0].Visible = false;
                con.Close();
            }
        }

        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxMessage.Clear();
            richTextBox.Clear();
            if (isFirstselect)
            {
                ClearPanels(false);
                labeluserName.Text = dataGridView.CurrentRow.Cells[0].Value.ToString();
                int id = Convert.ToInt32(dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[1].Value);
                labelId.Text = id.ToString();
                isFirstselect = true; ;
            }
            else
            {
                labeluserName.Text = dataGridView.CurrentRow.Cells[0].Value.ToString();
                int id = Convert.ToInt32(dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[1].Value);
                labelId.Text = id.ToString();
                isFirstselect = false;
            }
        }

        private void ButtonViewProfile_Click(object sender, EventArgs e)
        {
            ClearPanels(true);
            labelId.Text = "Id";
            GetusersInformation();
        }
        private void TextBoxSearch_TextChanged(object sender, EventArgs e)
        {
            DataView View = new DataView(table);
            View.RowFilter = string.Format(" Name like '%{0}%'", textBoxSearch.Text);
            dataGridView.DataSource = View;
        }

        private void GetusersInformation()
        {
            string userId = Logical.GetUserId(LoggedinUser);
            using (SqlConnection con = new SqlConnection(SQLCON))
            using (SqlCommand command = new SqlCommand("GetUserDatails", con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@UserId", SqlDbType.Int).Value = Convert.ToInt32(userId);
                command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50);
                command.Parameters["@FirstName"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@MiddelName", SqlDbType.NVarChar, 50);
                command.Parameters["@MiddelName"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@SurName", SqlDbType.NVarChar, 50);
                command.Parameters["@SurName"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@Gmail", SqlDbType.NVarChar, 130);
                command.Parameters["@Gmail"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@Contact", SqlDbType.BigInt);
                command.Parameters["@Contact"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@DateOfBirth", SqlDbType.Date);
                command.Parameters["@DateOfBirth"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    command.ExecuteNonQuery();
                    textBoxFirstName.Text = command.Parameters["@FirstName"].Value.ToString().Trim();
                    textBoxMiddleName.Text = command.Parameters["@MiddelName"].Value.ToString().Trim();
                    textBoxSurName.Text = command.Parameters["@SurName"].Value.ToString().Trim();
                    textBoxGmail.Text = command.Parameters["@Gmail"].Value.ToString().Trim();
                    textBoxContactNo.Text = command.Parameters["@Contact"].Value.ToString().Trim();
                    dateTimePicker.Value = Convert.ToDateTime(command.Parameters["@DateOfBirth"].Value.ToString().Trim());
                    con.Close();
                }
            }
        }

        private void ButtonLogOff_Click(object sender, EventArgs e)
        {
            LoginForm login = new LoginForm();
            login.Show();
            this.Hide();
        }

        private void ButtonUpdateInfo_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxFirstName.Text) && !string.IsNullOrEmpty(textBoxSurName.Text) && !string.IsNullOrEmpty(textBoxGmail.Text) && !string.IsNullOrEmpty(textBoxContactNo.Text))
            {
                try
                {
                    string userId = Logical.GetUserId(LoggedinUser);
                    DialogResult result = MessageBox.Show("Are you sure to update the details " + textBoxFirstName.Text + " " + textBoxMiddleName.Text + " " + textBoxSurName.Text + " ?", "Grant Permission", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        Logical.ExecuteNonQuery("Update UserInformation set FirstName='" + textBoxFirstName.Text + "',MiddleName='" + textBoxMiddleName.Text + "',Surname='" + textBoxSurName.Text + "',Gmail='" + textBoxGmail.Text + "',Contact=" + textBoxContactNo.Text + ",DateOfBirth='" + dateTimePicker.Text + "' Where UserId=" + userId);
                        MessageBox.Show("Update Success");
                    }
                    else
                    {

                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Server Down");
                }
            }
            else
            {
                MessageBox.Show("Please fill the details first!!");
            }
            GetusersInformation();
        }

        private void ButtonUpdateLogin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxUserName.Text) && !string.IsNullOrEmpty(textBoxPassword.Text) && !string.IsNullOrEmpty(textBoxRetypePassword.Text))
            {
                if (textBoxPassword.Text == textBoxRetypePassword.Text)
                {
                    DialogResult result = MessageBox.Show("Are you sure to update the details " + textBoxUserName.Text + " ?", "Grant Permission", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        string userId = Logical.GetUserId(LoggedinUser);
                        string Id = "select LoginId from UserInformation where UserId=" + userId;
                        var data = Logical.GetTableByQuery(Id);
                        int LoginId = Convert.ToInt32(data.Rows[0][0]);
                        Logical.ExecuteNonQuery("Update [LOGIN] set  UserName=HASHBYTES('Md5','" + textBoxUserName.Text + "'),[Password]=HASHBYTES('Md5','" + textBoxRetypePassword.Text + "') Where LoginId=" + LoginId);
                        MessageBox.Show("Update Success");
                    }
                    else
                    {

                    }
                }
                else
                {
                    MessageBox.Show("Password not matched Please retype password!!");
                    textBoxPassword.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please fill the details first!!");
            }
        }// Not in use

        private void MessangerPortal_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
