﻿using Messanger.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Messanger
{
   public class Logical
    {
       public static string SQLCON = System.Configuration.ConfigurationManager.ConnectionStrings["Connection"].ConnectionString.ToString();
       static string Available,Load;
        public static string Generate()
        {
            int minilenth = 8;
            int maxlenth = 12;
            string generat;
            string charavailable = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ123456789";
            StringBuilder password = new StringBuilder();
            Random random = new Random();
            int passwordlength = random.Next(minilenth, maxlenth + 1);
            while (passwordlength-- > 0)
            {
                password.Append(charavailable[random.Next(charavailable.Length)]);
            }
            generat = password.ToString();
            return generat;
        }
        public static SqlConnection DBConnect()
        {
            var conn = new SqlConnection("Data Source=.;Initial Catalog=MessageBox;Integrated Security=True");
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            return conn;
        }
        public static DataTable GetTableByQuery(string SqlQuery)
        {
            try
            {
                SqlCommand command = new SqlCommand();
                command.Connection = DBConnect();
                command.CommandText = SqlQuery;
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter =
                new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static void ExecuteNonQuery(string SqlQuery)
        {
            try
            {
                SqlCommand command = new SqlCommand();
                command.Connection = DBConnect();
                command.CommandText = SqlQuery;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
        }
       private static string Availability(string userName)
        {
            
            using (SqlConnection con = new SqlConnection(SQLCON))
            using (SqlCommand command = new SqlCommand("CheckUserAvailableStatus", con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@username", SqlDbType.NVarChar, 70).Value = userName;
                command.Parameters.Add("@UserAvailableStatus", SqlDbType.NVarChar, 20);
                command.Parameters["@UserAvailableStatus"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    command.ExecuteNonQuery();
                    Available = command.Parameters["@UserAvailableStatus"].Value.ToString().Trim();
                    con.Close();
                }
            }
            return Available;
        }

        private static string LoginCheck(string userName, string Password)
        {
            using (SqlConnection con = new SqlConnection(SQLCON))
            using (SqlCommand command = new SqlCommand("CheckLoginStatus", con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@userName", SqlDbType.NVarChar, 70).Value = userName;
                command.Parameters.Add("@password", SqlDbType.NVarChar, 70).Value = Password;
                command.Parameters.Add("@loginStatus", SqlDbType.Char, 20);
                command.Parameters["@loginStatus"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    command.ExecuteNonQuery();
                    Load = command.Parameters["@loginStatus"].Value.ToString().Trim();
                    con.Close();
                }
            }
            return Load;
        }
        public static bool CreateUser(string FirstName,string Middelname,string Surname,string Dob,String Contact,string Gmail,string userName, string Password)
        {
            bool result = false;
            using (SqlConnection con = new SqlConnection(SQLCON))
            using (SqlCommand command = new SqlCommand("CreateUser", con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 30).Value=FirstName;
                command.Parameters.Add("@MiddelName", SqlDbType.NVarChar, 30).Value = Middelname;
                command.Parameters.Add("@SurName", SqlDbType.NVarChar, 30).Value = Surname;
                command.Parameters.Add("@Gmail", SqlDbType.NVarChar, 60).Value = Gmail;
                command.Parameters.Add("@Contact", SqlDbType.NVarChar, 20).Value = Contact;
                command.Parameters.Add("@DateOfBirth", SqlDbType.Date).Value = Dob;
                command.Parameters.Add("@UserName", SqlDbType.NVarChar, 30).Value = userName;
                command.Parameters.Add("@Password", SqlDbType.NVarChar, 30).Value = Password;             
                try
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                        command.ExecuteNonQuery();
                        result = true;
                        con.Close();
                    }
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        public static string CheckUserLogin(string userName, string Password)
        {
           string Available, login, result;
            Available = Availability(userName);
            login = LoginCheck(userName, Password);
            if (Available == "Available" && login == "Success")
            {
                result = "Available_Success";
            }
            else if (Available == "Available" && login == "Failure")
            {
                result = "Available_Failure";
            }
            else
            {
                result = "Unavailable _Failure";
            }
            return result;
        }

        public static string GetUserId(string userName)
        {
            using (SqlConnection con = new SqlConnection(SQLCON))
            using (SqlCommand command = new SqlCommand("GetUserId", con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@userName", SqlDbType.NVarChar, 70).Value = userName;
                command.Parameters.Add("@userId", SqlDbType.BigInt);
                command.Parameters["@userId"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    command.ExecuteNonQuery();
                    Load = command.Parameters["@userId"].Value.ToString().Trim();
                    con.Close();
                }
            }
            return Load;
        }

    }
}
