USE [master]
GO
EXEC master.sys.xp_create_subdir 'D:\Database\'
GO
DROP DATABASE IF EXISTS [MessageBox]
GO
USE [master]
GO
/****** Object:  Database [MessageBox]    Script Date: 4/24/2019 1:54:16 PM ******/
CREATE DATABASE [MessageBox]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MessageBox', FILENAME = N'D:\Database\MessageBox.mdf' , SIZE = 12288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Secondary] 
( NAME = N'MessageBox_Secondary', FILENAME = N'D:\Database\MessageBox_Secondary.ndf' , SIZE = 12288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Ternary] 
( NAME = N'MessageBox_Ternary', FILENAME = N'D:\Database\MessageBox_Ternary.ndf' , SIZE = 12288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MessageBox_log', FILENAME = N'D:\Database\MessageBox_log.ldf' , SIZE = 12288KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [MessageBox] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MessageBox].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MessageBox] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MessageBox] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MessageBox] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MessageBox] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MessageBox] SET ARITHABORT OFF 
GO
ALTER DATABASE [MessageBox] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MessageBox] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MessageBox] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MessageBox] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MessageBox] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MessageBox] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MessageBox] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MessageBox] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MessageBox] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MessageBox] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MessageBox] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MessageBox] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MessageBox] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MessageBox] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MessageBox] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MessageBox] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MessageBox] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MessageBox] SET RECOVERY FULL 
GO
ALTER DATABASE [MessageBox] SET  MULTI_USER 
GO
ALTER DATABASE [MessageBox] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MessageBox] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MessageBox] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MessageBox] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MessageBox] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MessageBox] SET QUERY_STORE = OFF
GO
USE [MessageBox]
GO
/*====================================================================================*/
/*						Foreign Key: FK_MessageBox_REFRENCE_Files 					  */
/*====================================================================================*/		
IF EXISTS (SELECT 1
	FROM sys.sysreferences R JOIN sys.sysobjects O ON (O.id =R.constid AND O.type='F')
	WHERE R.fkeyid=OBJECT_ID('MessageBox') AND O.name='FK_MessageBox_REFRENCE_Files')
ALTER TABLE MessageBox
	DROP CONSTRAINT FK_MessageBox_REFRENCE_Files
GO
/*====================================================================================*/
/*						Foreign Key: FK_UserInformation_REFRENCE_Login 				  */
/*====================================================================================*/
IF EXISTS (SELECT 1
	FROM sys.sysreferences R JOIN sys.sysobjects O ON (O.id =R.constid AND O.type='F')
	WHERE R.fkeyid=OBJECT_ID('UserInformation') AND O.name='FK_UserInformation_REFRENCE_Login')
ALTER TABLE UserInformation
	DROP CONSTRAINT FK_UserInformation_REFRENCE_Login
GO
/*====================================================================================*/
/*								INDEX: IndexLogin 				 					  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('LOGIN')
			AND name ='IndexLogin'
			AND indid>0
			AND indid<255)
	DROP INDEX [LOGIN].IndexLogin
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('LOGIN')
			AND type='U')
	DROP TABLE [LOGIN]
GO
/*====================================================================================*/
/*								INDEX: IndexUserInformation 	 					  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('UserInformation')
			AND name ='IndexUserInformation'
			AND indid>0
			AND indid<255)
	DROP INDEX UserInformation.IndexUserInformation
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('UserInformation')
			AND type='U')
	DROP TABLE UserInformation
GO
/*====================================================================================*/
/*								INDEX: IndexFiles 				 					  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('Files')
			AND name ='IndexFiles'
			AND indid>0
			AND indid<255)
	DROP INDEX Files.IndexFiles
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('Files')
			AND type='U')
	DROP TABLE Files
GO
/*====================================================================================*/
/*								INDEX: IndexMessageBox 								  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('MessageBox')
			AND name ='IndexMessageBox'
			AND indid>0
			AND indid<255)
	DROP INDEX MessageBox.IndexMessageBox
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('MessageBox')
			AND type='U')
	DROP TABLE MessageBox
GO
/*====================================================================================*/
/*								Table: [LOGIN]  				 					  */
/*====================================================================================*/
CREATE TABLE [LOGIN](
		LoginId     INT				PRIMARY KEY IDENTITY(1,1),
		UserName	VARBINARY(20)	NOT NULL UNIQUE,
		[Password]	VARBINARY(20)	NOT NULL,
		IsOnline	BIT	DEFAULT 0   NOT NULL,
		LastOnline	DATETIME		NOT NULL DEFAULT GETDATE()
);
GO
/*====================================================================================*/
/*								Index: IndexLogin          				 		      */
/*====================================================================================*/
CREATE INDEX IndexLogin ON [LOGIN](
UserName ASC,
IsOnline ASC,
LastOnline ASC
);
GO

/*====================================================================================*/
/*								Table: UserInformation  				 			  */
/*====================================================================================*/
CREATE TABLE UserInformation(
		UserId		INT				PRIMARY KEY IDENTITY(1,1),
		FirstName	NVARCHAR(40)	NOT NULL,
		MiddleName	NVARCHAR(40)	NULL,
		Surname		NVARCHAR(40)	NOT NULL,
		Gmail       NVARCHAR(120)	NOT NULL UNIQUE,
		Contact		BIGINT			NOT NULL UNIQUE,
		DateOfBirth DATE			NOT NULL,
		LoginId     INT	
);
GO
/*====================================================================================*/
/*								Index: IndexUserInformation          				  */
/*====================================================================================*/
CREATE INDEX IndexUserInformation ON UserInformation(
LoginId ASC,
UserId ASC,
Gmail ASC,
Contact ASC
);

GO
/*====================================================================================*/
/*								Table: Files  				 				    	  */
/*====================================================================================*/
CREATE TABLE Files(
		FileId			INT				PRIMARY KEY IDENTITY(1,1),
		[FileName]		NVARCHAR(MAX)	NOT NULL,
		FileSize		DECIMAL			NOT NULL,
		FileExtension	NVARCHAR(10)	NOT NULL,	
		Attachment		VARBINARY(MAX)	NOT NULL, 
		[Location]		NVARCHAR(MAX)	NOT NULL,
		DateUploaded	Date			NOT NULL
);
GO
/*====================================================================================*/
/*								Index: IndexFiles                   				  */
/*====================================================================================*/
CREATE INDEX IndexFiles ON Files(
FileId ASC,
FileSize ASC,
FileExtension ASC,
DateUploaded ASC
);

GO
/*====================================================================================*/
/*								Table: MessageBox  				 					  */
/*====================================================================================*/
CREATE TABLE MessageBox(
		MessageId	INT				PRIMARY KEY IDENTITY(1,1),
		FromUserId	INT				NOT NULL,
		ToUserId	INT				NOT NULL,
		[Message]	NVARCHAR(MAX)	NOT NULL,
		FileId		INT				NULL,
		TimeSent	DATETIME		NOT NULL,
		IsRead		BIT	DEFAULT 0	NOT NULL
);
GO
/*====================================================================================*/
/*								Index: IndexMessageBox                 				  */
/*====================================================================================*/
CREATE INDEX IndexMessageBox ON MessageBox(
MessageId ASC,
FileId ASC,
TimeSent ASC,
IsRead ASC
);

GO
ALTER TABLE MessageBox
	ADD CONSTRAINT FK_MessageBox_REFRENCE_Files FOREIGN KEY (FileId)
		REFERENCES Files (FileId)
GO
ALTER TABLE UserInformation
	ADD CONSTRAINT FK_UserInformation_REFRENCE_Login FOREIGN KEY (LoginId)
		REFERENCES [LOGIN] (LoginId)
GO
USE MessageBox
GO
-- Procedure to check whether that name is in database or not
CREATE PROCEDURE CheckUserAvailableStatus
(		
		@userName NVARCHAR(70),
		@UserAvailableStatus VARCHAR(20) OUTPUT
)
AS 
BEGIN
	IF EXISTS (SELECT UserName FROM [LOGIN] WHERE UserName=HASHBYTES('Md5',@userName))
		SET @UserAvailableStatus='Available'
	ELSE
		SET @UserAvailableStatus='Unavailable'
	PRINT @UserAvailableStatus
END
GO
USE MessageBox
GO
-- Procedure to Validate user login
CREATE PROCEDURE CheckLoginStatus
(
	    @userName NVARCHAR(70),
		@password NVARCHAR(70),
		@loginStatus VARCHAR(20) OUTPUT
)
AS
BEGIN
	IF EXISTS (SELECT UserName FROM [LOGIN] WHERE UserName=HASHBYTES('Md5',@userName) AND [Password]=HASHBYTES('Md5',@password))
		SET @loginStatus='Success'
	ELSE
		SET @loginStatus='Failure'
	PRINT @loginStatus
END
GO
USE MessageBox
GO
-- Procedure to insert user informations
CREATE PROCEDURE CreateUser
(
		   @FirstName NVARCHAR(50),
		   @MiddelName NVARCHAR(50),
		   @SurName NVARCHAR(50),
		   @Gmail NVARCHAR(130),
		   @Contact BIGINT,
		   @DateOfBirth DATE,
		   @UserName NVARCHAR(30),
           @Password NVARCHAR(30)	  
)
AS
BEGIN
DECLARE @LoginID INT
BEGIN TRY
    BEGIN TRANSACTION
INSERT INTO [LOGIN](UserName, [Password], IsOnline) 
VALUES(HASHBYTES('Md5',@UserName), HASHBYTES('Md5',@Password), 1);          --insert values into the [LOGIN] table
SELECT @LoginID = SCOPE_IDENTITY();						   --get the identity value by using SCOPE_IDENTITY() or @@IDENTITY
														   --insert values into the second table 
insert into UserInformation(FirstName, MiddleName, Surname, Gmail,Contact,DateOfBirth,LoginId) 
values(@FirstName, @MiddelName, @SurName, @Gmail,@Contact,@DateOfBirth,@LoginID);
COMMIT TRANSACTION -- Transaction Success!
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
ROLLBACK TRANSACTION --RollBack in case of Error
DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
DECLARE @ErrorState INT = ERROR_STATE()
-- Use RAISERROR inside the CATCH block to return error  
-- information about the original error that caused  
-- execution to jump to the CATCH block.  
RAISERROR (@ErrorMessage, -- Message text.  
   @ErrorSeverity, -- Severity.  
   @ErrorState -- State.  
   );
END CATCH
END
GO
--EXEC CreateUser @Firstname='Binay',@MiddelName='',@SurName='Karki',@Gmail='KarkiNiya@gmail.com',@Contact=234545687,@DateOfBirth='1999-12-12',@UserName='Binay',@Password='123b';
CREATE PROCEDURE GetUserId
(
	    @userName NVARCHAR(70),
		@userId BIGINT OUTPUT
)
AS
Declare @loginId BIGINT 
BEGIN
	(SELECT @loginId=LoginId FROM [LOGIN] WHERE UserName=HASHBYTES('Md5',@userName))
	set @userId=(select UserId from UserInformation where LoginId=@loginId)
	PRINT @userId
END
GO
