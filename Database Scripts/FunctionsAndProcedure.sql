
-- Procedure to check whether that name is in database or not
CREATE PROCEDURE CheckUserAvailableStatus
(		
		@userName NVARCHAR(70),
		@UserAvailableStatus VARCHAR(20) OUTPUT
)
AS 
BEGIN
	IF EXISTS (SELECT UserName FROM [LOGIN] WHERE UserName=@userName)
		SET @UserAvailableStatus='Available'
	ELSE
		SET @UserAvailableStatus='Unavailable'
	PRINT @UserAvailableStatus
END
GO
-- Procedure to Validate user login
CREATE PROCEDURE CheckLoginStatus
(
	    @userName NVARCHAR(70),
		@password NVARCHAR(70),
		@loginStatus VARCHAR(20)
)
AS
BEGIN
	IF EXISTS (SELECT UserName FROM [LOGIN] WHERE UserName=@userName AND [Password]=HASHBYTES('Md5',@password))
		SET @loginStatus='Sccess'
	ELSE
		SET @loginStatus='Failure'
	PRINT @loginStatus
END
GO
-- Procedure to insert user informations
CREATE PROCEDURE CreateUser
(
		   @FirstName NVARCHAR(50),
		   @MiddelName NVARCHAR(50),
		   @SurName NVARCHAR(50),
		   @Gmail NVARCHAR(130),
		   @Contact INT,
		   @DateOfBirth DATE,
		   @UserName NVARCHAR(30),
           @Password NVARCHAR(30)	  
)
AS
BEGIN
DECLARE @LoginID INT
BEGIN TRY
    BEGIN TRANSACTION
INSERT INTO [LOGIN](UserName, [Password], IsOnline,LastOnline) 
VALUES(@UserName, HASHBYTES('Md5',@Password), 1,GETDATE());          --insert values into the [LOGIN] table
SELECT @LoginID = SCOPE_IDENTITY();						   --get the identity value by using SCOPE_IDENTITY() or @@IDENTITY
														   --insert values into the second table 
insert into UserInformation(FirstName, MiddleName, Surname, Gmail,Contact,DateOfBirth,LoginId) 
values(@FirstName, @MiddelName, @SurName, @Gmail,@Contact,@DateOfBirth,@LoginID);
COMMIT TRANSACTION -- Transaction Success!
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
ROLLBACK TRANSACTION --RollBack in case of Error
DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
DECLARE @ErrorState INT = ERROR_STATE()
-- Use RAISERROR inside the CATCH block to return error  
-- information about the original error that caused  
-- execution to jump to the CATCH block.  
RAISERROR (@ErrorMessage, -- Message text.  
   @ErrorSeverity, -- Severity.  
   @ErrorState -- State.  
   );
END CATCH
end
